import React, { useState, useEffect } from "react";
import axios  from "axios";
import BlogApp from "./BlogApp";

const TestAxios = () => {
  const [Data, setData] = useState(undefined);

  const getAllData = () => {
    var config = {
      method: "get",
      url: "http://localhost:3004/postgenerated",
      header: {},
    };
    

    axios(config).then((value) => {
      setData(value.data);
    });
  };
  

  useEffect(() => {
    getAllData();
  }, []);

//   const renderData = () => {
//     return Data?.map((value, index) => {
//       return (
//         <div key={index}>
//           <p>Title: {value.title}</p>
//           <p>Img: {value.img}</p>
//           <hr></hr>
//         </div>
//       );
//     });
//   };

    return (
      <BlogApp data={Data} />
    );

};

export default TestAxios;
