import Avatar from "@mui/material/Avatar";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import { red } from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import React from "react";
import Navbar from "../components/Navbar";
import { Grid, Item } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";

const BlogApp = (props) => {
  return (
    <div>
      <Navbar />
      <div className="content">
        <Grid container spacing={0}>
          {props.data?.map((value, index) => {
            return (
              <Grid xs={12} md={6}>
                <Card sx={{ maxWidth: 345 }}>
                  <CardHeader
                    avatar={<Avatar sx={{ bgcolor: red[500] }}>R</Avatar>}
                    title={value.title}
                    subheader={value.datePost}
                  />
                  <CardMedia component="img" height="194" image={value.img} />
                  <CardContent>
                    <Typography variant="body2" color="text.secondary">
                      {value.description}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            );
          })}
        </Grid>

        <List
          sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper"}}
        >
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <ImageIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Photos" secondary="Jan 9, 2014" />
          </ListItem>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <WorkIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Work" secondary="Jan 7, 2014" />
          </ListItem>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <BeachAccessIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Vacation" secondary="July 20, 2014" />
          </ListItem>
        </List>
      </div>
    </div>
  );
};

export default BlogApp;
