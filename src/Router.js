import React from 'react'
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import About from './page/About'
import TestAxios from './page/Axios'
import BlogApp from './page/BlogApp'
import Login from './page/Login'
import Memory from './page/Memory'

const Routers = () => {
  return (
    <div>
      <Router>
        <Switch>
            <Route exact path={"/"}>
                <TestAxios />
            </Route>

            <Route exact path={"/about"}>
                <About />
            </Route>

            <Route exact path={"/memory"}>
                <Memory />
            </Route>
            <Route exact path={"/login"}>
                <Login />
            </Route>
            {/* <Route exact path={"/axios"}>
                <TestAxios/>
            </Route> */}
        </Switch>
      </Router>
    </div>
  )
}

export default Routers